# Introduction

This repository mainly focuses on the contribution files to the summer project 2021 and DolphinScheduler.

The file catagory is as follows:

```
--mid
-----Apache DolphinScheduler 架构演进介绍及开源经验分享 - eBay 阮文俊.html
-----DS_architecture_evolution.md
-----flink-call_en-us_.md
-----flink-call_zh-cn_.md
-----open-api_en-us_.md
-----open-api_zh-cn_.md
--final
-----DolphinScheduler_parameter_introduction.md
-----DolphinScheduler_参数使用说明.md
-----dev-quick-start.md
-----软硬件环境建议配置.md
--README.md
```





# Details

There are 2 folders, mid is used for the conclusion of project in the middle stage, and final is used for the final contribution.


| **Apache DolphinScheduler 架构演进介绍及开源经验分享 - eBay 阮文俊.html** | **An article uploaded to WeChat Official accounts**          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **DS_architecture_evolution.md**                             | **A blog introducing the evolution steps of DS architecture, which is merged to original repository.** |
| **flink-call_en-us_.md**                                     | **A doc introducing the steps of calling a flink(en-us), which is merged to original repository.** |
| **flink-call_zh-cn_.md**                                     | **A doc introducing the steps of calling a flink(zh-cn), which is merged to original repository.** |
| **open-api_en-us_.md**                                       | **A doc introducing the steps of calling a api(en-us), which is merged to original repository.** |
| **open-api_zh-cn_.md**                                       | **A doc introducing the steps of calling a api(zh-cn), which is merged to original repository.** |
| **DolphinScheduler_parameter_introduction.md**               | **A doc introducing the paramters(en-us), created a PR.** |
| **DolphinScheduler_参数使用说明.md**                          | **A doc introducing the paramters(zh-cn), created a PR.** |
| **dev-quick-start.md**                                       | **A doc updating for the required package, created a PR.** |
| **软硬件环境建议配置.md**                                      | **A doc updating for the java setting, created a PR.** |


# **Contributor**

**Contributor name is Xu Weiyi, a third year student in Tianjin university, majoring in computer science and technology.**
